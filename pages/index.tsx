import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import TopBar from "../components/TopBar";
import Header from "../components/Header";
import Main from "../components/Main";

const Home: NextPage = () => {
    return (
        <div className="px-[10px] md:px-[99px] py-7 relative">
            <TopBar />
            <Header />
            <div className="h-[333px]"> </div>
            <Main />
        </div>
    );
};

export default Home;
