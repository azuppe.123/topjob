import React from "react";
import Filters from "../components/Filters";
import Sidebar from "../components/Sidebar";
import Center from "../components/Center"

type Props = {};

const Main = (props: Props) => {
    return (
        <div className="grid grid-cols-4 h-full gap-[72px] pt-8">
            <div className="hidden md:block  md:col-span-1">
                <Filters />
            </div>
            <div className="col-span-4 md:col-span-2 bg-white"><Center/></div>
            <div className="hidden md:block col-span-1">
                <Sidebar />
            </div>
        </div>
    );
};

export default Main;
