import React from "react";

type Props = {};

const Header = (props: Props) => {
    return (
        <div aria-hidden="true" className=" inset-0 overflow-hidden absolute top-[90px] h-[330px]">
            <div
                className="relative  bg-no-repeat w-full object-cover object-center h-[330px]"
                style={{
                    backgroundImage: `url("bg.png")`,
                    backgroundSize: "cover",
                }}
            >
                <div className=" mx-auto max-w-3xl  flex items-center h-[330px]  text-center ">
                    <div>
                        <h1 className="text-5xl  font-bold text-white">Top jobs board for professionals</h1>
                        <p className="mt-4 base font-normal text-white">
                            Discover your next career move with over 15 000 opening vacancies, customer support, sowtware, design anywhere in the world or remotely.
                        </p>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Header;
