import React from "react";
import DropDown from "../components/DropDown";

type Props = {};
const filters = [
    {
        id: "1",
        name: "All Offers",
    },
    {
        id: "2",
        name: "Most Relevant",
    },
    {
        id: "3",
        name: "Recommended",
    },
    {
        id: "4",
        name: "Most Recent",
    },
];

const Filters = (props: Props) => {
    return (
        <div className="space-y-[20px]">
            <div>
                <label className="text-base font-bold text-gray-900">Notifications</label>

                <fieldset className="mt-3">
                    <div className="space-y-4">
                        {filters.map((filter) => (
                            <div key={filter.id} className="flex items-center">
                                <input id={filter.id} name="filter" type="radio" defaultChecked={filter.id === "1"} className="h-4 w-4  text-violet-600 " style={{ accentColor: "rgb(124 58 237)" }} />
                                <label htmlFor={filter.id} className="ml-3 block text-sm font-medium text-gray-700">
                                    {filter.name}
                                </label>
                            </div>
                        ))}
                    </div>
                </fieldset>
            </div>

            <div>
                <label className="text-base font-bold text-gray-900">Location</label>
                <fieldset className="mt-3">
                    <div>
                        <input
                            type="email"
                            name="email"
                            id="email"
                            className=" px-3 pt-2 pb-3 border block w-full rounded text-gray-300 border-gray-300 shadow-sm bg-white "
                            placeholder="Location..."
                        />
                    </div>
                </fieldset>
            </div>

            <div>
                <label className="text-base font-bold text-gray-900">Type of Work</label>
                <p className="text-sm leading-5 text-gray-500">How do you prefer to receive notifications?</p>
                <fieldset className="mt-3">
                    <div className="space-y-4">
                        <DropDown />
                    </div>
                </fieldset>
            </div>

            <div>
                <label className="text-base font-bold text-gray-900">Industries</label>
                <p className="text-sm leading-5 text-gray-500">How do you prefer to receive notifications?</p>
                <fieldset className="mt-3">
                    <div className="space-y-4">
                        <DropDown />
                    </div>
                </fieldset>
            </div>
        </div>
    );
};

export default Filters;
