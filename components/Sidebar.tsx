import React from "react";
import DropDown from "../components/DropDown";

type Props = {};
const items = [
    {
        id: "1",
        name: "Template Factory",
        job_title: "UI/UX Designer",
    },
    {
        id: "2",
        name: "AgencyAnalytics",
        job_title: "Frontend Developer (Website)",
    },
    {
        id: "3",
        name: "Clevertech",
        job_title: "Senior Staff React Developer",
    },
    {
        id: "4",
        name: "Practice",
        job_title: "UI/UX Designer",
    },
];

const topCompanies = [
    {
        id: 1,
        name: "Template Factory",
        url: "/loop.png",
    },
    {
        id: 1,
        name: "AgencyAnalytics",
        url: "/analitica.png",
    },
];

const Sidebar = (props: Props) => {
    return (
        <div className="space-y-[20px]">
            <div>
                <label className="text-base font-bold text-gray-900">Recent View</label>

                <fieldset className="mt-3">
                    <div className="space-y-2">
                        {items.map((item) => (
                            <div key={item.id} className="items-center">
                                <p className=" block text-xs font-normal text-gray-700">{item.name}</p>
                                <p className="text-xs font-medium text-gray-900">{item.job_title}</p>
                            </div>
                        ))}
                    </div>
                </fieldset>
            </div>

            <div>
                <label className="text-base font-bold text-gray-900">Top Companies</label>
                <div className=" mt-3">
                    {topCompanies.map((item) => {
                        return (
                            <div className="flex items-center" key={item.id}>
                                <div className="flex-shrink-0">
                                    <img className="h-4 w-4 rounded-full" src={item.url} alt="logo" />
                                </div>
                                <div className="ml-[9px]">
                                    <p className="text-xs font-medium leading-6 text-gray-900">{item.name}</p>
                                </div>
                            </div>
                        );
                    })}
                </div>
            </div>

            <div>
                <label className="text-base font-bold text-gray-900">Type of Work</label>
                <p className="text-sm leading-5 text-gray-500">How do you prefer to receive notifications?</p>
                <fieldset className="mt-4">
                    <div className="space-y-4">
                        <DropDown />
                    </div>
                </fieldset>
            </div>
        </div>
    );
};

export default Sidebar;
