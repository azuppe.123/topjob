import { CheckCircleIcon, MagnifyingGlassIcon, ChevronRightIcon, EnvelopeIcon, HeartIcon } from "@heroicons/react/24/outline";
import React from "react";
import Search from "./ui/Search";
// import Icon from "../components/icon/Icon.jsx";

type Props = {};

const applications = [
    {
        applicant: {
            name: "Ricardo Cooper",
            email: "ricardo.cooper@example.com",
            imageUrl: "https://images.unsplash.com/photo-1472099645785-5658abf4ff4e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
        },
        date: "2020-01-07",
        dateFull: "January 7, 2020",
        stage: "Completed phone screening",
        status: "New",
        badge: "Fully Remote",
    },
    {
        applicant: {
            name: "Kristen Ramos",
            email: "kristen.ramos@example.com",
            imageUrl: "https://images.unsplash.com/photo-1550525811-e5869dd03032?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
        },
        date: "2020-01-07",
        dateFull: "January 7, 2020",
        stage: "Completed phone screening",
        status: "1 d",
        badge: false,
    },
    {
        applicant: {
            name: "Ted Fox",
            email: "ted.fox@example.com",
            imageUrl: "https://images.unsplash.com/photo-1500648767791-00dcc994a43e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=facearea&facepad=2&w=256&h=256&q=80",
        },
        date: "2020-01-07",
        dateFull: "January 7, 2020",
        stage: "Completed phone screening",
        status: "1 d",
        badge: false,
    },
];

const Center = (props: Props) => {
    return (
        <div className="space-y-4 md:space-y-0">
            <div className="block md:hidden w-full my-4 ">
                <div className="border-gray-600 border rounded py-4 text-center text-sm font-medium text-gray-700">
                    <div className=" flex items-center justify-center gap-1">
                        <span>Filters</span>
                        <div className="h-4 w-4 bg-violet-600 rounded-full text-white font-normal text-[10px] flex items-center justify-center mt-[1px]">
                            <span>1</span>
                        </div>
                    </div>
                </div>
            </div>
            <div className="shadow rounded">
                <div className="border-b border-gray-200">
                    <Search />
                </div>
                <div>
                    <div className="overflow-hidden bg-white shadow sm:rounded-md">
                        <ul role="list" className="divide-y divide-gray-200">
                            {applications.map((application) => (
                                <li key={application.applicant.email}>
                                    <div className="block hover:bg-gray-50 px-4 py-4 sm:px-6">
                                        <div className=" min-w-0  items-center space-y-4">
                                            <div className="flex-shrink-0 flex items-center justify-between">
                                                <div className="flex items-center gap-3">
                                                    <img className="h-12 w-12 rounded-full" src={application.applicant.imageUrl} alt="" />
                                                    <p className="truncate text-sm font-medium text-gray-700">{application.applicant.name}</p>
                                                    <p className=" text-sm text-gray-400">• Shamheel</p>
                                                </div>
                                                <p className={`${application.status === "new" ? "text-violet-700" : "text-gray-400"} text-xs font-normal`}>{application.status}</p>
                                            </div>
                                            <div className="flex justify-between items-center">
                                                <div>
                                                    <h1 className=" text-lg font-bold text-gray-900">UI/UX Designer</h1>
                                                    <p className=" text-sm font-normal text-gray-500">Marketplace apps similar to olx or bigl but for EU market</p>
                                                    <div className="mt-2">
                                                        {application.badge ? (
                                                            <span className="py-1  pl-[10px] pr-[13px] text-xs font-normal text-teal-600 bg-teal-50 rounded-full">{application.badge}</span>
                                                        ) : null}
                                                    </div>
                                                </div>
                                                <HeartIcon className=" w-4 h-4  text-gray-400" />
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            ))}
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Center;
