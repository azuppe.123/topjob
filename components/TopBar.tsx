import React, { useState } from "react";
import Button from "../components/ui/Button";

type Props = {};

const navigations = [
    { id: 1, name: "Find a Job" },
    { id: 2, name: "Categories" },
    { id: 3, name: "Community" },
];

const TopBar: React.FC<Props> = ({}) => {
    const [mobileMenuopen, setMobileMenuOpen] = useState(false);
    return (
        <nav className="flex justify-between items-center mb-6">
            <div className="flex items-center gap-[14px]">
                <img src="/logo.png" alt="logo" />
                <div className="text-xs font-normal text-gray-400 flex items-center justify-center">Find the job that fits you the best</div>
            </div>
            <div className="hidden md:flex gap-6 items-center mr-2">
                {navigations.map((item) => {
                    return (
                        <p key={item.id} className="text-sm font-normal text-gray-500">
                            {item.name}
                        </p>
                    );
                })}
                <Button
                    text="Post a job"
                    bg_color="bg-violet-600"
                    text_color="text-white"
                    onClick={(val: boolean) => {
                        console.log(val);
                    }}
                />
            </div>
        </nav>
    );
};

export default TopBar;
