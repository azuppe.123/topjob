import { Fragment, useState, useEffect } from "react";
import { Listbox, Transition } from "@headlessui/react";
import { ChevronDownIcon } from "@heroicons/react/20/solid";

const people = [
    { id: 1, name: "Wade Cooper" },
    { id: 2, name: "Arlene Mccoy" },
    { id: 3, name: "Devon Webb" },
    { id: 4, name: "Tom Cook" },
    { id: 5, name: "Tanya Fox" },
    { id: 6, name: "Hellen Schmidt" },
    { id: 7, name: "Caroline Schultz" },
    { id: 8, name: "Mason Heaney" },
    { id: 9, name: "Claudie Smitham" },
    { id: 10, name: "Emil Schaefer" },
];

export default function DropDown() {
    const [selected, setSelected] = useState<{ id: Number; name: String }>();
    const [list, setList] = useState<String[]>([]);
    const [open, setOpen] = useState<boolean>(false);

    const handleSelect = (item: { id: Number; name: String }) => {
        setSelected(item);
    };

    useEffect(() => {
        if (!selected) {
            return;
        }
        setList([...list, selected.name]);
    }, [selected]);

    return (
        <Listbox value={selected} onChange={(val: any) => handleSelect(val)}>
            <div className="relative ">
                <Listbox.Button
                    className="relative w-full cursor-default rounded border border-gray-300 bg-white text-gray-300 pl-3 pt-2 pb-3 pr-10 text-left shadow-sm "
                    onClick={() => setOpen(!open)}
                >
                    <span className="block truncate">{list.length && list.length < 2 ? list.join(", ") : list.length > 2 ? list.slice(0, 2).join(", ") : "Select"}</span>
                    <span className="pointer-events-none absolute inset-y-0 right-0 flex items-center pr-2">
                        <ChevronDownIcon className="h-5 w-5 text-gray-400" aria-hidden="true" />
                    </span>
                </Listbox.Button>

                <Transition show={open} as={Fragment} leave="transition ease-in duration-100" leaveFrom="opacity-100" leaveTo="opacity-0">
                    <Listbox.Options className="absolute z-10 mt-1 max-h-60 w-full overflow-auto rounded-md bg-white py-1 text-base shadow-lg ring-1 ring-black ring-opacity-5 focus:outline-none sm:text-sm">
                        {people.map((person) => (
                            <Listbox.Option key={person.id} className={"text-gray-900 relative cursor-default select-none py-2 pl-8 pr-4"} value={person}>
                                <div className="flex gap-1">
                                    <div className="flex h-5 items-center">
                                        <input
                                            id="candidates"
                                            aria-describedby="candidates-description"
                                            name="candidates"
                                            type="checkbox"
                                            className="h-4 w-4 rounded border-gray-300 text-indigo-600 focus:ring-indigo-500"
                                        />
                                    </div>
                                    <span className={"font-normal block truncate"}>{person.name}</span>
                                </div>
                            </Listbox.Option>
                        ))}
                    </Listbox.Options>
                </Transition>
            </div>
        </Listbox>
    );
}
