import React from "react";

interface Props {
    bg_color: String;
    text_color: String;
    text: String;
    onClick: Function;
}

const Button: React.FC<Props> = ({ bg_color = "bg-violet-600", text, text_color = "text-white", onClick = () => {} }) => {
    return <button className={`px-4 py-2 rounded text-sm font-medium ${bg_color} ${text_color}`}>{text}</button>;
};

export default Button;
