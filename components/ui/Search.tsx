import React from "react";
import { CheckCircleIcon, MagnifyingGlassIcon, ChevronDownIcon, EnvelopeIcon } from "@heroicons/react/20/solid";

type Props = {};

const Search = (props: Props) => {
    return (
        <div className="flex justify-between px-5">
            <div className="mt-1  relative ">
                <span className="pointer-events-none absolute inset-y-0 left-0 flex items-center pr-2">
                    <MagnifyingGlassIcon className="h-5 w-5 text-gray-400" aria-hidden="true" />
                </span>
                <input type="text" name="name" id="name" className="block w-full border-0 text-gray-300 py-3 pl-10 border-transparent   text-base font-normal" placeholder="Start searching..." />
            </div>

            <p className="text-gray-500 text-sm font-medium flex items-center gap-1 cursor-pointer">
                <span className="font-normal">Sort by : </span>
                <span>Latest</span> <ChevronDownIcon className="text-gray-500 h-4 w-4" />
            </p>
        </div>
    );
};

export default Search;
